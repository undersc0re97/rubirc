=begin
              _      _
             | |    (_)
 _ __  _   _ | |__   _  _ __   ___
| '__|| | | || '_ \ | || '__| / __|
| |   | |_| || |_) || || |   | (__
|_|    \__,_||_.__/ |_||_|    \___|

A Ruby IRC bot library that aims to be minimalistic
and not be a sack of horse shit.


=end
# Copyright (c) 2013 Ethan Best <underscr97@gmail.com>

# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
# TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

require "socket"

class Rubirc
  class Bot
    def initialize(svr_name, host, port, use_ssl)
      @svr_name  = svr_name
      @host     = host
      @port     = port
      @use_ssl  = use_ssl
    end

    def register(nick, ident, real_name, use_id, id_service, id_password)
      @nick         = nick
      @ident        = ident
      @real_name    = real_name
      @use_id       = use_id
      @id_service   = id_service
      @id_password  = id_password
    end
  end

  class Socket
    #
  end
end
